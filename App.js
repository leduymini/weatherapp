import React from 'react';
import { Button, Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer,
} from 'react-navigation';

import DetailsScreen from './DetailsScreen';
import ControlScreen from './ControlScreen';
import HomeScreen from './HomeScreen';
import SettingScreen from './SettingScreen';

const HomeStack = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Details: { screen: DetailsScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#498fff',
      },
      headerTitleStyle: {
        textAlign: 'center', flexGrow: 1,
        alignSelf: 'center',
      },
      headerTintColor: '#FFFFFF',
      title: 'Onsen Weather',
    },
  }
);

const ControlStack = createStackNavigator(
  {

    Controls: { screen: ControlScreen },

  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Controls',
    },
  }
);

const SettingsStack = createStackNavigator(
  {

    Setting: { screen: SettingScreen },

  },
  {
    defaultNavigationOptions: {
      // headerStyle: {
      //   backgroundColor: '#42f44b',
      // },
      // headerTintColor: '#FFFFFF',
      // title: 'Setting',
    },
    tabBarOptions: {
      activeBackgroundColor: '#498fff',
      inactiveBackgroundColor: '#498fff',
    }
  }
);
const App = createBottomTabNavigator(
  {
    'Weather Details': { screen: HomeStack },
    Controls: { screen: ControlStack },
    Setting: { screen: SettingsStack },

  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Weather Details') {
          iconName = require('./assets/cloud_icon.png')
        }
        else if (routeName === 'Controls') {
          iconName = require('./assets/light_icon.png')
        }
        else if (routeName === 'Setting') {
          iconName = require('./assets/setting_icon.png')
        }
        return <Image source={iconName} resizeMode={'contain'} style={{ width: 20, height: 20, tintColor: '#fff' }} />;
      },
    }),
    tabBarOptions: {
      showIcon: true,
      activeTintColor: '#fff',
      inactiveTintColor: '#d4ddd7',
      activeBackgroundColor: '#2f8c61',
      inactiveBackgroundColor: '#2f8c61',
      labelStyle: {
        fontSize: 13,
      },
    },
  }
);

export default createAppContainer(App);