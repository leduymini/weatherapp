import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, FlatList, ScrollView, TextInput, TouchableHighlight, Image } from 'react-native';
import { Button } from 'react-native-elements';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      data: [
        {
          city: 'Ho Chi Minh',
          temperature: '29',
          status: 'Sun',
          humidity: '66',
        },
        {
          city: 'Ha Noi',
          temperature: '20',
          status: 'Rain',
          humidity: '70',
        },
        {
          city: 'Da Nang',
          temperature: '30',
          status: 'Wind',
          humidity: '67',
        },
        {
          city: 'Hue',
          temperature: '25',
          humidity: '49',
        },

      ]
    };
  }

  AddItemsToArray = () => {
    this.state.data.push(this.state.text.toString());
    this.setState({
      data: this.state.data,
    })
  }

  delete = (index) => {
    const lastItem = this.state.data[index];
    this.shiftItems(index);
    this.setState({
      data: this.state.data,
    })
    // return lastItem;
  }

  shiftItems = (index) => {
    for (let i = index; i < this.state.data.length - 1; i++) {
      this.state.data[i] = this.state.data[i + 1];
    }
    delete this.state.data[this.state.data.length - 1];
    this.state.data.length--;
  }

  // deleteItem = (index) => {
  //   this.state.data.delete(index);
  //   this.setState({
  //     data: this.state.data,
  //   })
  // }

  renderItem = ({ item, index }) => {
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <TouchableOpacity style={{ flexDirection: 'row' }}
          onPress={() => this.props.navigation.navigate('Details', { city: item.city, temperature: item.temperature, status: item.status, humidity: item.humidity })}
        >

          {item.status === 'Sun'
            ?
            <Image source={require('./assets/sun_icon.png')} style={{ width: 30, marginLeft: 20, height: 30, alignSelf: 'center' }} resizeMode={'contain'} />
            : item.status === 'Rain'
              ?
              <Image source={require('./assets/rain_icon.png')} style={{ width: 30, marginLeft: 20, height: 30, alignSelf: 'center' }} resizeMode={'contain'} />
              : item.status === 'Wind'
                ?
                <Image source={require('./assets/wind_icon.png')} style={{ width: 30, marginLeft: 20, height: 30, alignSelf: 'center' }} resizeMode={'contain'} />
                :
                <Image source={require('./assets/default_icon.png')} style={{ width: 30, marginLeft: 20, height: 30, alignSelf: 'center' }} resizeMode={'contain'} />
          }

          <View style={{ marginLeft: 20, flex: 1 }}>
            <View key={item} style={{ flexDirection: 'row', height: 70, justifyContent: 'space-between', alignItems: 'center' }}>
              <View>
                <Text style={{ fontWeight: 'bold', }}>{item.city}</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text>{item.temperature}°C</Text>
                  <Text style={{ marginLeft: 5 }}>{item.humidity}%</Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <TouchableOpacity>
                  <Image source={require('./assets/refresh_icon.png')} resizeMode={'contain'} style={{ height: 20, width: 20, marginRight: 20 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.delete(index)}>
                  <Image source={require('./assets/delete_icon.png')} resizeMode={'contain'} style={{ height: 20, width: 20, marginRight: 20, }} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ borderWidth: 0.2 }} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }


  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#dee1e5' }}>
        {/* <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white' }}
            onChangeText={(text) => this.setState({ text })}
            value={this.state.text}
            placeholder={'Insert todo'}
          /> */}
        {/* <Button
            title="INSERT TODO"
            containerStyle={{ marginTop: 10, marginBottom: 10 }}
            onPress={this.AddItemsToArray}
          /> */}
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
        />

        <View style={{ borderTopWidth: 0.8, alignItems: 'center' }}>
          <TouchableOpacity style={{ flex: 1, justifyContent: "center" }}>
            <Text style={{ color: '#4286f4', fontSize: 20, alignSelf: 'center', marginVertical: 30 }}>+ ADD LOCATION</Text>
          </TouchableOpacity>
        </View>
      </ScrollView >
    );
  }
}
const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});