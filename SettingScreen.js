import React from 'react';
import { Text, View, Animated } from 'react-native';
import { LinearTextGradient } from "react-native-text-gradient";

let randomHex = () => {
  let letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export default class SettingScreen extends React.Component {
  constructor(props) {
    super(props)
    this.Animation = new Animated.Value(0);
    this.state = {
      backgroundColor: randomHex()
    };
  }

  componentDidMount() {
    this.StartBackgroundColorAnimation();
  }

  StartBackgroundColorAnimation = () => {
    this.Animation.setValue(0);

    Animated.timing(
      this.Animation,
      {
        toValue: 1,
        duration: 3000
      }
    ).start(() => { this.StartBackgroundColorAnimation() });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    }
  }
  render() {

    const BackgroundColorConfig = this.Animation.interpolate(
      {
        inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1],
        outputRange: ['#FFEB3B', '#CDDC39', '#009688', '#03A9F4', '#3F51B5', '#98FFFC']
      });

    return (
      <View style={{ flex: 1, backgroundColor: '#e5e7ea' }}>
        <View style={{ marginLeft: 10 }}>
          <Text style={{ marginTop: 70, color: '#cb58d8', fontSize: 25, fontWeight: 'bold' }}>Weather</Text>
          <Text style={{ fontSize: 20, color: '#000', marginTop: 20, fontWeight: 'bold' }}>Temperature</Text>
          <Text style={{ marginTop: 5, fontSize: 16 }}>Current Information: °C</Text>

          <Text style={{ fontSize: 20, color: '#000', marginTop: 15, fontWeight: 'bold' }}>Network Refresh</Text>
          <Text style={{ marginTop: 5, fontSize: 16 }}>Current Information: Real-time</Text>

          <Text style={{ marginTop: 25, color: '#cb58d8', fontSize: 25, fontWeight: 'bold' }}>ABOUT</Text>
          <Text style={{ fontSize: 20, color: '#000', marginTop: 20, fontWeight: 'bold' }}>Instructor</Text>

          <View style={{ marginTop:20,flexDirection: 'row' }}>

            <View>
              <Text style={{ fontSize: 30,fontWeight:"bold" }}>Student:</Text>
              <Text style={{ fontSize: 30, fontWeight:"bold" }}>ID:</Text>
            </View>

            <View>
              {/* <LinearTextGradient
                style={{ fontWeight: "bold", fontSize: 30, marginLeft: 20 }}
                locations={[0, 1]}
                colors={["green", "blue"]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              >
                <Text>LÊ DUY</Text>
              </LinearTextGradient> */}
              <Animated.Text style={{fontWeight: "bold", fontSize: 30, marginLeft: 20 ,color: BackgroundColorConfig }}>LÊ DUY</Animated.Text>
              
              <Animated.Text style={{fontWeight: "bold", fontSize: 30, marginLeft: 20 ,color: BackgroundColorConfig }}>1552064</Animated.Text>
            </View>

          </View>
        </View>
      </View>
    );
  }
}